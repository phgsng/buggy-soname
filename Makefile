subdirs = main dependency
inputs = $(foreach subdir, $(subdirs), $(subdir)/build.rs $(subdir)/Cargo.toml $(subdir)/src/lib.rs)
targets = $(foreach subdir, $(subdirs), $(subdir)/target)
libmain = main/target/debug/libmain.so

all: wrong-soname

wrong-soname: $(libmain)
	readelf -d $(libmain) |grep SONAME

$(libmain): $(inputs)
	pushd main; cargo build --verbose

clean:
	rm -fr -- $(targets)

.PHONY: clean
