Run ``make``.

Context:

- https://github.com/lu-zero/cdylib-link-lines/issues/9
- https://users.rust-lang.org/t/suppress-link-arg-in-dependency/89245/2
